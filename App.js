import React from 'react';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import {StyleSheet, View, Text} from "react-native";
import reducer from "./store/reducer";
import Calculator from "./containers/Calculator/Calculator";
import {StatusBar, Platform} from "react-native";

const store = createStore(reducer);

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <StatusBar barStyle = "dark-content"/>
        <View style={styles.container}>
          <Calculator/>
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    backgroundColor: '#fff',
    paddingTop: Platform === "ios" ? 0 : StatusBar.currentHeight,
  }
});