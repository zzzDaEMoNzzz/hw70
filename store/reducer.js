import {CALC_ADD, CALC_CLEAR, CALC_GET_RESULT, CALC_REMOVE} from "./actionTypes";

const initialState = {
  calcString: '',
  needClear: null
};

const parseCalcString = calcString => {
  let result = calcString;

  const calcStringNums = result.split(/[+-/*]/g);

  calcStringNums.forEach(str => {
    const includesPow = str.includes('²');
    const includesSqrt = str.includes('√');

    let num = str.replace('²', '').replace('√', '');

    if (includesPow) {
      if (str.indexOf('²') === str.length - 1) {
        const powResult = Math.pow(parseInt(num), 2);
        result = result.replace(str, String(powResult));
        num = String(powResult);
      }
    }

    if (includesSqrt) {
      if (str.indexOf('√') === 0) {
        const sqrtResult = Math.sqrt(parseInt(num));
        if (includesPow) {
          result = result.replace(num, String(sqrtResult));
        } else {
          result = result.replace(str, String(sqrtResult));
        }
      }
    }
  });

  return result;
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CALC_ADD:
      return {
        ...state,
        calcString: state.needClear ? action.value : state.calcString + action.value,
        needClear: null
      };
    case CALC_REMOVE:
      return {
        ...state,
        calcString: state.needClear ? '' : state.calcString.slice(0, -1),
        needClear: null
      };
    case CALC_CLEAR:
      return {
        ...state,
        calcString: ''
      };
    case CALC_GET_RESULT:
      let result = '';
      let error = null;

      if (state.calcString.length > 0) {
        const calcString = parseCalcString(state.calcString);

        try {
          result = eval(calcString);
        } catch (e) {
          error = e;
          result = 'Error!';
        }
      }

      return {
        ...state,
        calcString: String(result),
        needClear: error,
      };
    default:
      return state;
  }
};

export default reducer;