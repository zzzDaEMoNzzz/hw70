import {CALC_ADD, CALC_CLEAR, CALC_GET_RESULT, CALC_REMOVE} from "./actionTypes";

export const calcAdd = value => ({type: CALC_ADD, value});
export const calcRemove = () => ({type: CALC_REMOVE});
export const calcClear = () => ({type: CALC_CLEAR});
export const calcGetResult = () => ({type: CALC_GET_RESULT});