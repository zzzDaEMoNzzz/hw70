export const CALC_ADD = 'CALC_ADD';
export const CALC_REMOVE = 'CALC_REMOVE';
export const CALC_CLEAR = 'CALC_CLEAR';
export const CALC_GET_RESULT = 'CALC_GET_RESULT';