import React, {Component, Fragment} from 'react';
import {Text, View, ScrollView, TouchableOpacity} from "react-native";
import {connect} from "react-redux";

import styles from './styles';
import {calcAdd, calcClear, calcGetResult, calcRemove} from "../../store/actions";



class Calculator extends Component {
  render() {
    const calculatorLayout = [
      [
        {value: 'x²', style: styles.calcButton_green, handler: () => this.props.calcAdd('²')},
        {value: '√', style: styles.calcButton_green, handler: () => this.props.calcAdd('√')},
        {value: '%', style: styles.calcButton_green, handler: () => this.props.calcAdd('%')},
        {value: '/', style: styles.calcButton_green, handler: () => this.props.calcAdd('/')},
      ],
      [
        {value: '1', handler: () => this.props.calcAdd('1')},
        {value: '2', handler: () => this.props.calcAdd('2')},
        {value: '3', handler: () => this.props.calcAdd('3')},
        {value: '*', style: styles.calcButton_green, handler: () => this.props.calcAdd('*')},
      ],
      [
        {value: '4', handler: () => this.props.calcAdd('4')},
        {value: '5', handler: () => this.props.calcAdd('5')},
        {value: '6', handler: () => this.props.calcAdd('6')},
        {value: '-', style: styles.calcButton_green, handler: () => this.props.calcAdd('-')},
      ],
      [
        {value: '7', handler: () => this.props.calcAdd('7')},
        {value: '8', handler: () => this.props.calcAdd('8')},
        {value: '9', handler: () => this.props.calcAdd('9')},
        {value: '+', style: styles.calcButton_green, handler: () => this.props.calcAdd('+')},
      ],
      [
        {value: '.', handler: () => this.props.calcAdd('.')},
        {value: '0', handler: () => this.props.calcAdd('0')},
        {value: '<', style: styles.calcButton_orange, handler: () => this.props.calcRemove(), onLongPress: () => this.props.calcClear()},
        {value: '=', style: styles.calcButton_orange, handler: () => this.props.calcGetResult()},
      ],
    ];

    const calculatorButtons = calculatorLayout.reduce((array, row) => {
      const buttons = row.map((btn, index) => (
        <TouchableOpacity
          key={index}
          style={[
            styles.calcButton,
            btn.style || null
          ]}
          onPress={btn.handler ? btn.handler : null}
          onLongPress={btn.onLongPress ? btn.onLongPress : null}
        >
          <Text style={styles.calcButtonText}>{btn.value}</Text>
        </TouchableOpacity>
      ));

      array.push(
        <View key={array.length} style={styles.calcButtonsRow}>
          {buttons}
        </View>
      );

      return array;
    }, []);

    return (
      <Fragment>
        <View>
          <ScrollView
            contentContainerStyle={styles.calcStringWrapper}
            showsHorizontalScrollIndicator={false}
            horizontal
            ref={ref => this.scrollView = ref}
            onContentSizeChange={() => {
              this.scrollView.scrollToEnd({animated: true});
            }}
          >
            <Text style={styles.calcString} >{this.props.calcString}</Text>
          </ScrollView>
        </View>
        <View style={styles.calcButtons}>
          {calculatorButtons}
        </View>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  calcString: state.calcString
});

const mapDispatchToProps = dispatch => ({
  calcAdd: value => dispatch(calcAdd(value)),
  calcRemove: () => dispatch(calcRemove()),
  calcClear: () => dispatch(calcClear()),
  calcGetResult: () => dispatch(calcGetResult()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Calculator);