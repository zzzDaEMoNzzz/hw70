import {StyleSheet} from "react-native";

export default StyleSheet.create({
  calcStringWrapper: {
    height: 70,
    minWidth: '100%',
    backgroundColor: '#fa0',
  },
  calcString: {
    textAlign: 'right',
    fontSize: 40,
    height: 70,
    lineHeight: 70,
    minWidth: '100%',
  },
  calcButtons: {
    flex: 1,
    flexDirection: 'column',
  },
  calcButtonsRow: {
    flex: 1,
    flexDirection: 'row',
  },
  calcButton: {
    flex: 1,
    width: '20%',
    backgroundColor: '#0af',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#fff',
    borderWidth: 1
  },
  calcButton_orange: {
    backgroundColor: '#fa0',
  },
  calcButton_green: {
    backgroundColor: '#5a0',
  },
  calcButtonText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#fff',
  },
});